CREATE DATABASE  IF NOT EXISTS `courseadminfinal` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `courseadminfinal`;
-- MySQL dump 10.13  Distrib 5.6.24, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: courseadminfinal
-- ------------------------------------------------------
-- Server version	5.6.24-0ubuntu2

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `course_module`
--

DROP TABLE IF EXISTS `course_module`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `course_module` (
  `course_id` int(10) unsigned NOT NULL,
  `module_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`module_id`,`course_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `course_module`
--

LOCK TABLES `course_module` WRITE;
/*!40000 ALTER TABLE `course_module` DISABLE KEYS */;
INSERT INTO `course_module` VALUES (2,1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(3,1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(4,1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(2,2,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(3,2,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(3,3,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(1,4,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(2,4,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(3,4,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(4,4,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(2,5,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(3,5,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(4,5,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(1,6,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(2,6,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(3,6,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(2,7,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(3,7,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(4,7,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(1,8,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(2,8,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(3,8,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(2,9,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(3,9,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(4,9,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(1,10,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(2,10,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(3,10,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(4,10,'0000-00-00 00:00:00','0000-00-00 00:00:00');
/*!40000 ALTER TABLE `course_module` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `courses`
--

DROP TABLE IF EXISTS `courses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `courses` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `leader` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `courses_code_unique` (`code`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `courses`
--

LOCK TABLES `courses` WRITE;
/*!40000 ALTER TABLE `courses` DISABLE KEYS */;
INSERT INTO `courses` VALUES (1,'Computing','BIS40674993',5,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(2,'Software & Systems','BIS12301443',10,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(3,'Web','BIS10655617',4,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(4,'Application Development','BIS43921738',5,'0000-00-00 00:00:00','0000-00-00 00:00:00');
/*!40000 ALTER TABLE `courses` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `item_module`
--

DROP TABLE IF EXISTS `item_module`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `item_module` (
  `item_id` int(10) unsigned NOT NULL,
  `module_id` int(10) unsigned NOT NULL,
  `complete` tinyint(1) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`module_id`,`item_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `item_module`
--

LOCK TABLES `item_module` WRITE;
/*!40000 ALTER TABLE `item_module` DISABLE KEYS */;
INSERT INTO `item_module` VALUES (1,1,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(2,1,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(3,1,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(4,1,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(5,1,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(14,1,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(20,1,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(23,1,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(31,1,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(33,1,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(40,1,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(1,2,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(2,2,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(3,2,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(4,2,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(5,2,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(8,2,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(11,2,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(19,2,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(21,2,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(24,2,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(25,2,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(29,2,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(35,2,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(1,3,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(2,3,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(3,3,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(4,3,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(5,3,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(9,3,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(13,3,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(15,3,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(23,3,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(24,3,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(31,3,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(36,3,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(37,3,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(1,4,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(2,4,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(3,4,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(4,4,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(5,4,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(27,4,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(34,4,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(1,5,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(2,5,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(3,5,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(4,5,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(5,5,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(20,5,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(25,5,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(36,5,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(38,5,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(39,5,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(1,6,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(2,6,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(3,6,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(4,6,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(5,6,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(8,6,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(10,6,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(19,6,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(26,6,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(27,6,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(32,6,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(36,6,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(38,6,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(1,7,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(2,7,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(3,7,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(4,7,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(5,7,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(6,7,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(15,7,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(16,7,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(31,7,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(1,8,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(2,8,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(3,8,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(4,8,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(5,8,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(10,8,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(1,9,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(2,9,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(3,9,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(4,9,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(5,9,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(11,9,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(20,9,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(34,9,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(1,10,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(2,10,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(3,10,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(4,10,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(5,10,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(17,10,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(32,10,0,'0000-00-00 00:00:00','0000-00-00 00:00:00');
/*!40000 ALTER TABLE `item_module` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `items`
--

DROP TABLE IF EXISTS `items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `items` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `text` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `default` tinyint(1) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `items`
--

LOCK TABLES `items` WRITE;
/*!40000 ALTER TABLE `items` DISABLE KEYS */;
INSERT INTO `items` VALUES (1,'Dolor nesciunt id recusandae cupiditate omnis.',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(2,'Impedit occaecati nam exercitationem repellat et voluptatum.',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(3,'Non vel enim eius nesciunt laboriosam tenetur a sed.',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(4,'Veniam consectetur enim et rem inventore ad laboriosam.',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(5,'Est ut unde ut totam est consequuntur.',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(6,'Dolore est autem sit eum.',0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(7,'Eveniet totam consectetur quod voluptatem voluptate quibusdam.',0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(8,'Nesciunt sed et aut voluptas non ut possimus libero.',0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(9,'Ut ullam sit expedita.',0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(10,'Ducimus distinctio qui qui deserunt.',0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(11,'Fugit et vel ducimus.',0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(12,'Qui nam error modi.',0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(13,'Quaerat voluptatem aspernatur harum molestiae.',0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(14,'Et voluptatem cupiditate repellat est.',0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(15,'Exercitationem quaerat qui exercitationem porro et sit itaque.',0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(16,'Illum quia voluptatem exercitationem ipsum.',0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(17,'Exercitationem dicta expedita dolorem possimus quasi iure.',0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(18,'Distinctio illo mollitia id facere.',0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(19,'Possimus voluptate vel eum nulla aut praesentium.',0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(20,'Esse consequatur doloribus sapiente facilis quis.',0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(21,'Impedit quos sunt eius laboriosam quos adipisci nisi.',0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(22,'Nisi et aut officiis nisi maxime.',0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(23,'Animi ut nemo perferendis nihil sed fugiat nihil.',0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(24,'Ducimus voluptatibus laudantium animi nihil nulla.',0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(25,'Itaque nulla cum aut doloremque.',0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(26,'Culpa aliquid inventore tenetur ex sit tempore ea.',0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(27,'Blanditiis voluptatem dolor sint saepe id dicta perspiciatis.',0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(28,'Laboriosam tempora dignissimos ea earum possimus laborum.',0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(29,'Ad atque voluptatum repellendus enim.',0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(30,'Tempora est aut laudantium sunt necessitatibus enim vel.',0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(31,'Iste commodi deleniti delectus natus possimus.',0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(32,'Quae aperiam ut ex vel provident.',0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(33,'Illo rerum aut accusantium commodi eveniet.',0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(34,'Eaque nihil consequatur odio et dolorem ea.',0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(35,'Et voluptates consequatur qui qui.',0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(36,'Sed est qui praesentium.',0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(37,'Dolores deserunt dolores voluptatem praesentium excepturi nostrum possimus.',0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(38,'Error ipsum et adipisci molestiae consequatur.',0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(39,'Vero labore sunt ipsam nihil deserunt qui.',0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(40,'Error numquam esse enim et et.',0,'0000-00-00 00:00:00','0000-00-00 00:00:00');
/*!40000 ALTER TABLE `items` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES ('2014_10_12_000000_create_users_table',1),('2014_10_12_100000_create_password_resets_table',1),('2015_07_21_110723_create_courses_table',1),('2015_07_21_110733_create_modules_table',1),('2015_07_21_110813_create_items_table',1),('2015_07_21_110847_create_itemmodule_table',1),('2015_07_21_110855_create_coursemodule_table',1);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `modules`
--

DROP TABLE IF EXISTS `modules`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `modules` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `leader` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `modules_code_unique` (`code`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `modules`
--

LOCK TABLES `modules` WRITE;
/*!40000 ALTER TABLE `modules` DISABLE KEYS */;
INSERT INTO `modules` VALUES (1,'Module 1','CIS2582',2,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(2,'Module 2','CIS3273',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(3,'Module 3','CIS3897',9,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(4,'Module 4','CIS4805',8,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(5,'Module 5','CIS2545',2,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(6,'Module 6','CIS3552',9,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(7,'Module 7','CIS2731',7,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(8,'Module 8','CIS3829',7,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(9,'Module 9','CIS3777',2,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(10,'Module 10','CIS3511',10,'0000-00-00 00:00:00','0000-00-00 00:00:00');
/*!40000 ALTER TABLE `modules` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  KEY `password_resets_email_index` (`email`),
  KEY `password_resets_token_index` (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_resets`
--

LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `admin` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'Pedro Senger','Bella38@gmail.com','18dZ9KBeXp','YiRP6FQQWK','2015-08-02 14:46:28','2015-08-02 14:46:28',0),(2,'Garland Stark','vHagenes@DuBuque.com','hvdRxEw0RB','TH8YvYhhe5','2015-08-02 14:46:28','2015-08-02 14:46:28',0),(3,'Ms. Brandy Mraz','Antonia14@Zemlak.com','vPzLQacwoi','WDJNH3M1oR','2015-08-02 14:46:28','2015-08-02 14:46:28',0),(4,'Dr. Kacey Erdman V','Whitney.Mohr@Gaylord.biz','oyjBStsu9D','EbHxtoIeRI','2015-08-02 14:46:29','2015-08-02 14:46:29',0),(5,'Barrett Kovacek Jr.','Haleigh08@gmail.com','IJkTkN3nBM','ljXibNqQW5','2015-08-02 14:46:29','2015-08-02 14:46:29',0),(6,'Quinten Weissnat II','Garrick92@Lakin.com','u2estuTDiF','Nvc8XPHV3t','2015-08-02 14:46:29','2015-08-02 14:46:29',0),(7,'Jolie Toy','Jacynthe97@Hamill.com','UJn4XKtGUs','0458dZA1dk','2015-08-02 14:46:29','2015-08-02 14:46:29',0),(8,'Imani Buckridge','mProsacco@Spencer.org','UvnJcwHA0Q','CzKoHUBoqh','2015-08-02 14:46:29','2015-08-02 14:46:29',0),(9,'Ariane Wisoky','Oceane63@yahoo.com','jnu1wWCbdG','yzpsyjCKX7','2015-08-02 14:46:29','2015-08-02 14:46:29',0),(10,'Macey Lang','Elvis.Kautzer@Bashirian.biz','gERnpy80tA','QG1uvZynRU','2015-08-02 14:46:29','2015-08-02 14:46:29',0),(11,'admin','admin@example.com','$2y$10$EzrzFJ/B5Xd/7z9hPHt4fOfjNKnsA9KrrBfyq290Ky2aGyzoNJuf.',NULL,'0000-00-00 00:00:00','0000-00-00 00:00:00',1),(12,'notadmin','notadmin@example.com','$2y$10$LHNcIGnLmjAFu26sli6aG.mto99qD8egsW7FIdUMcEPrLlxhNAzw.',NULL,'0000-00-00 00:00:00','0000-00-00 00:00:00',0);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-08-02 16:48:09

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ItemModule extends Model
{
    protected $table = 'item_module';
    protected $fillable = ['module_id', 'item_id', 'completed'];

    public function courses()
    {
        return $this->belongsTo('App\Course')->withTimestamps();
    }

    public function items()
    {
        return $this->belongsTo('App\Item')->withTimestamps()->orderBy('text', 'asc');
    }

}

<header>
        <nav class="top-bar" data-topbar role="navigation">
            <ul class="title-area">
            <li class="name">
                <h1><a href="/">CourseAdmin</a></h1>
            </li>
        </ul>

        @if (Auth::check())
            <section class="top-bar-section">
                <!-- Right Nav Section -->
                <ul class="right">
                    <li class="active"><a href="/admin/courses">Courses</a></li>
                    <li class="active"><a href="/admin/modules">Modules</a></li>
                    <li class="active"><a href="/admin/items">Items</a></li>
                    <li class=""></li>
                    <li><a href="/admin/logout">Log Out</a></li>

                </ul>

                <!-- Left Nav Section -->
                <ul class="left">
                    <li><a href="/admin/dash">Dashboard</a></li>
                </ul>
            </section>
            @else
            <section class="top-bar-section">
                <!-- Right Nav Section -->
                <ul class="right">
                    <li class="active">{!! link_to('/admin/login', 'Log In') !!}</li>
                </ul>
            </section>
        @endif
    </nav>
</header>